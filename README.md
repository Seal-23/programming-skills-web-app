# programming-skills-web-app

An Angulat 4 Web App.

## Steps to run the Web App.

#### 1. Install node modules.
#
> Make sure you has installed node.js and npm in your operative system.

Run in the root project folder:

```sh
npm install
```

#### 2. Run API.


Follow readme steps to run this [API](https://gitlab.com/Seal-23/programming-skills-api).


#### 3. Run aplication in localhost.
#

Run in the root project folder:

```sh
npm start
```