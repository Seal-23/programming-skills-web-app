import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddpersonComponent } from './components/tools/addperson/addperson.component';
import { ListpeopleComponent } from './components/tools/listpeople/listpeople.component';


const routes: Routes = [
  { path: 'add-person', component: AddpersonComponent },
  { path: 'list-people', component: ListpeopleComponent },
  { path: '**', redirectTo: 'add-person' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
