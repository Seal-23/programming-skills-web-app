import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { catchError } from 'rxjs/operators';
import { of } from "rxjs";
import swal, { SweetAlertIcon } from 'sweetalert2';


class petitionConfig {
  showErrorAlert: boolean = true;
  alertType: SweetAlertIcon = "error";
  alertTitle: string = 'Error';
  filter: any = {};
  where: any = {};
  isCount: boolean = false;
  data: any = {};

  constructor(params: Partial<petitionConfig>) {
    for (const key in params) {
      this[key] = params[key];
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  get(endPoint: string, config_: Partial<petitionConfig> = {}): Observable<any> {
    const config = new petitionConfig(config_)
    const url = environment.apiUrl + endPoint + (config.isCount ? '/count?where=' + JSON.stringify(config.where) : '?filter=' + JSON.stringify(config.filter))
    return this.http.get(url).pipe(
      catchError((error) => {
        if (config.showErrorAlert) {
          swal.fire({
            icon: config.alertType,
            title: config.alertTitle,
            text: !!error.message ? error.message : 'Presentamos problemas tecnicos en caso de persistir contacte a soporte'
          })
        }
        return of(null);
      })
    )
  }

  post(endPoint: string, config_: Partial<petitionConfig> = {}): Observable<any> {
    const config = new petitionConfig(config_)
    const url = environment.apiUrl + endPoint;
    return this.http.post(url, config.data).pipe(
      catchError((error) => {
        if (config.showErrorAlert) {
          swal.fire({
            icon: config.alertType,
            title: config.alertTitle,
            text: !!error.error.error.message ? error.error.error.message : 'Presentamos problemas tecnicos en caso de persistir contacte a soporte'
          })
        }
        return of(null);
      })
    )
  }
}
