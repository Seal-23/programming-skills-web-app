import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit {

  sideBarRoutes = [
    { path: '/add-person', displayName: 'Añadir persona' },
    { path: '/list-people', displayName: 'Ver personas' },
  ]

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

}
