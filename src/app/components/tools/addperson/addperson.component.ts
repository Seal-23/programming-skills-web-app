import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { ApiService } from "../../../services/api.service";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert2';


const initPagination: any = {
  currentPage: 1,
  totalPages: 1,
  loading: false
}

@Component({
  selector: 'app-addperson',
  templateUrl: './addperson.component.html',
  styleUrls: ['./addperson.component.sass']
})

export class AddpersonComponent implements OnInit {

  addPersonForm = new FormGroup({
    fullname: new FormControl('', [Validators.required]),
    identificationNumber: new FormControl('', [Validators.required]),
    birth: new FormControl('', [Validators.required]),
    gender: new FormControl(null, [Validators.required]),
  });

  selectedParents = {
    male: null,
    female: null
  }

  isSubmitted: boolean = false;


  genders = [{ label: 'Femenino', value: 'female' }, { label: 'Masculino', value: 'male' }]

  parentesPaginationData = {
    male: [],
    maleColors: [],
    female: [],
    femaleColors: []
  }

  parentsPaginationConfig = {
    pageSize: 6,
    emptyPageMessage: 'Aun no hay personas para seleccionar',
    male: initPagination,
    female: initPagination
  }

  parents = [{ title: 'Madre', gender: 'female' }, { title: 'Padre', gender: 'male' }]

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.initPagination('male');
    this.initPagination('female');
  }

  initPagination(gender: 'male' | 'female') {
    this.parentsPaginationConfig[gender] = initPagination;
    this.parentsPaginationConfig[gender].loading = true;
    const pagConfig = this.parentsPaginationConfig[gender];
    const pageSize = this.parentsPaginationConfig.pageSize;
    const where = { gender: gender }
    const filter = { where, limit: pageSize, skip: pageSize * (pagConfig.currentPage - 1) }
    const dataPetiton = this.api.get('/people', { filter });
    const coutPetiton = this.api.get('/people', { isCount: true, where });
    forkJoin([dataPetiton, coutPetiton]).subscribe((responses) => {
      this.parentesPaginationData[gender] = responses[0];
      this.parentesPaginationData[gender + 'Colors'] = responses[0].map(() => this.randomColor());
      this.parentsPaginationConfig[gender] = {
        ...this.parentsPaginationConfig[gender],
        totalPages: Math.floor(responses[1].count / pageSize) + 1,
        loading: false
      }
    })
  }

  space2Plus(sentence: string) {
    return sentence.replace(/\s/g, "+")
  }

  randomColor() {
    const rangeSize = 100;
    const RgbParts = [
      Math.floor(Math.random() * 256),
      Math.floor(Math.random() * rangeSize),
      Math.floor(Math.random() * rangeSize) + 256 - rangeSize
    ].sort((a, b) => Math.random() - 0.5);
    return RgbParts.map(p => ('0' + p.toString(16)).substr(-2)).join('');
  }

  selectParent(parent) {
    this.selectedParents[parent.gender] = parent;
  }

  removeSelect(gender: 'male' | 'female') {
    this.selectedParents[gender] = null;
  }

  updateParentList(gender: 'male' | 'female') {
    this.parentsPaginationConfig[gender].loading = true;
    const pagConfig = this.parentsPaginationConfig[gender];
    const pageSize = this.parentsPaginationConfig.pageSize;
    const where = { gender: gender }
    const filter = { where, limit: pageSize, skip: pageSize * (pagConfig.currentPage - 1) }
    this.api.get('people', { filter }).subscribe((data) => {
      this.parentesPaginationData[gender] = data;
      this.parentesPaginationData[gender + 'Colors'] = data.map(() => this.randomColor())
      this.parentsPaginationConfig[gender].loading = false;
    })
  }

  posiblePages(gender: 'male' | 'female'): Array<number> {
    const pages = [];
    const pageToView = 3; //Odd number
    const totalPages = this.parentsPaginationConfig[gender].totalPages;
    const currentPage = this.parentsPaginationConfig[gender].currentPage;
    const selectorPageSize: number = totalPages > pageToView ? pageToView : totalPages;
    let begin_page = 1;
    if ((totalPages > pageToView) && currentPage > Math.floor(pageToView / 2)) {
      const reference = totalPages - currentPage;
      if (reference > Math.floor(pageToView / 2)) begin_page = currentPage - Math.floor(pageToView / 2);
      else begin_page = currentPage + reference - 2 * Math.floor(pageToView / 2);
    }
    for (let page = begin_page; page < (begin_page + selectorPageSize); page++) {
      pages.push(page)
    }
    return pages;
  }

  previousPage(gender: 'male' | 'female') {
    const currentPage = this.parentsPaginationConfig[gender].currentPage;
    if (currentPage > 1) {
      this.parentsPaginationConfig[gender] = { ...this.parentsPaginationConfig[gender], currentPage: currentPage - 1 }
      this.updateParentList(gender);
    }
  }

  nextPage(gender: 'male' | 'female') {
    const totalPages = this.parentsPaginationConfig[gender].totalPages;
    const currentPage = this.parentsPaginationConfig[gender].currentPage;
    if (currentPage < totalPages) {
      this.parentsPaginationConfig[gender] = { ...this.parentsPaginationConfig[gender], currentPage: currentPage + 1 }
      this.updateParentList(gender);
    }
  }

  goToPage(gender: 'male' | 'female', page: number) {
    const totalPages = this.parentsPaginationConfig[gender].totalPages;
    if (page <= totalPages && page >= 1) {
      this.parentsPaginationConfig[gender] = { ...this.parentsPaginationConfig[gender], currentPage: page }
      this.updateParentList(gender);
    }
  }

  async createPerson() {
    this.isSubmitted = true;
    if (this.addPersonForm.valid) {
      const data = this.addPersonForm.value;
      data.birth = new Date(data.birth);
      if (!!this.selectedParents.male || !!this.selectedParents.female) {
        const relationWhere: any = {};
        relationWhere.womanId = this.selectedParents.female ? this.selectedParents.female.id : undefined;
        relationWhere.manId = this.selectedParents.male ? this.selectedParents.male.id : undefined;
        const relations = await this.api.get('relationships', { filter: { where: relationWhere } }).toPromise();
        if (!!relations.length) {
          data.parentsId = relations[0].id;
        } else {
          data.parentsId = (await this.api.post('relationships', { data: relationWhere }).toPromise()).id;
        }
      }
      this.api.post('people', { data }).subscribe((person) => {
        if (person) {
          this.isSubmitted = false;
          this.selectedParents = {
            male: null,
            female: null
          }
          this.updateParentList(person.gender);
          this.addPersonForm.setValue({
            fullname: '',
            identificationNumber: '',
            birth: '',
            gender: null
          });
          swal.fire({
            icon: 'success',
            title: "Persona creada con exito",
            text: `${person.fullname} ha sido almacenad${person.gender == "male" ? 'o' : 'a'} en la base de datos`
          });
        }
      })
    }
  }

}
