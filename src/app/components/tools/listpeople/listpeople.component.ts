import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { ApiService } from "../../../services/api.service";

@Component({
  selector: 'app-listpeople',
  templateUrl: './listpeople.component.html',
  styleUrls: ['./listpeople.component.sass']
})
export class ListpeopleComponent implements OnInit {

  paginationConfig = {
    pageSize: 6,
    currentPage: 1,
    totalPages: 1,
    loading: false
  }

  peopleTableData = [];

  genderTrans = {
    male: "Masculino",
    female: "Femenino"
  }

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.paginationConfig.loading = true;
    this.api.get('people', { isCount: true }).subscribe((count) => {
      this.paginationConfig.totalPages = Math.floor(count.count / this.paginationConfig.pageSize) + 1;
      this.getPeopleTableData();
    });
  }

  previousPage() {
    const currentPage = this.paginationConfig.currentPage;
    if (currentPage > 1) {
      this.paginationConfig = { ...this.paginationConfig, currentPage: currentPage - 1 }
      this.getPeopleTableData();
    }
  }

  nextPage() {
    const totalPages = this.paginationConfig.totalPages;
    const currentPage = this.paginationConfig.currentPage;
    if (currentPage < totalPages) {
      this.paginationConfig = { ...this.paginationConfig, currentPage: currentPage + 1 }
      this.getPeopleTableData();
    }
  }

  goToPage(page: number) {
    const totalPages = this.paginationConfig.totalPages;
    if (page <= totalPages && page >= 1) {
      this.paginationConfig = { ...this.paginationConfig, currentPage: page }
      this.getPeopleTableData();
    }
  }

  posiblePages(): Array<number> {
    const pages = [];
    const pageToView = 5; //Odd number
    const totalPages = this.paginationConfig.totalPages;
    const currentPage = this.paginationConfig.currentPage;
    const selectorPageSize: number = totalPages > pageToView ? pageToView : totalPages;
    let begin_page = 1;
    if ((totalPages > pageToView) && currentPage > Math.floor(pageToView / 2)) {
      const reference = totalPages - currentPage;
      if (reference > Math.floor(pageToView / 2)) begin_page = currentPage - Math.floor(pageToView / 2);
      else begin_page = currentPage + reference - 2 * Math.floor(pageToView / 2);
    }
    for (let page = begin_page; page < (begin_page + selectorPageSize); page++) {
      pages.push(page)
    }
    return pages;
  }

  getPeopleTableData() {
    this.paginationConfig.loading = true;
    const filter = { limit: this.paginationConfig.pageSize, skip: this.paginationConfig.pageSize * (this.paginationConfig.currentPage - 1) }
    this.api.get('people', { filter }).subscribe((people) => {
      const parentFilter = { include: ['man', 'woman'], where: { id: { inq: people.map((person) => person.parentsId) } } }
      console.log(parentFilter)
      const childFilter = {
        where: {
          or: people.map((person) => {
            return person.gander == "female" ? { womanId: person.id } : { manId: person.id }
          })
        },
        include: ['children']
      }
      const allChildrenRequest = this.api.get('relationships', { filter: childFilter })
      const allParentsRequest = this.api.get('relationships', { filter: parentFilter })
      forkJoin([allChildrenRequest, allParentsRequest]).subscribe((childrenAndParents) => {
        const allParents = childrenAndParents[1];
        const allChildren = childrenAndParents[0];
        this.peopleTableData = this.peopleTableData = people.map((person) => {
          const parents = allParents.find((parentRelation) => person.parentsId == parentRelation.id);
          let fatherName = '-';
          let motherName = '-';
          if (parents) {
            if (parents.woman) motherName = parents.woman.fullname;
            if (parents.man) fatherName = parents.man.fullname;
          }
          let childrenNamesArray = []
          allChildren.filter((ChildrensInRel) => (person.gender == "female" ? ChildrensInRel.womanId : ChildrensInRel.manId) == person.id).forEach((personRelationships) => {
            if (personRelationships.children) {
              childrenNamesArray.push(...personRelationships.children.map(child => child.fullname));
            }
          })
          if (!childrenNamesArray.length) childrenNamesArray = ['-']
          const childrenNames = childrenNamesArray.toString().replace(/([,])/g, ', ')
          return [
            person.fullname,
            person.identificationNumber,
            this.genderTrans[person.gender],
            new Date(person.birth).toLocaleDateString(),
            fatherName,
            motherName,
            childrenNames
          ]
        })
        this.paginationConfig.loading = false;
      })
    })
  }

}
