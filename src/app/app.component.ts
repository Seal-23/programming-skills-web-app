import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})


export class AppComponent {
  title = 'programming-skills-web-app';
  isOpenMenu = false;

  constructor(public router: Router) { }

  ngOnInit(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) this.isOpenMenu = false
    })
  }

  toogleMenu() {
    this.isOpenMenu = !this.isOpenMenu;
  }
}
